;; Transforms Org documents in the repository into an HTML website.
;;
;; Based on the work of Rasmus.

(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'org-ref) ;; bibliography support

;; Force publishing of unchanged files to make sure all the pages get published.
;; Otherwise, the files considered unmodified based on Org timestamps are not
;; published even if they were previously deleted from the publishing directory.
(setq org-publish-use-timestamps-flag nil)

;; Disable Babel code evaluation.
(setq org-export-babel-evaluate nil)
(setq org-confirm-babel-evaluate nil)

;; Preserve indentation on export and tangle.
(setq org-src-preserve-indentation t)

;; Use TAB to indent code in org mode code blocks.
(setq org-src-tab-acts-natively t)
(setq python-indent-guess-indent-offset t)

;; Include the Inria favicon in to the HTML header.
(setq org-html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")

;; Configure HTML website publishing.
(setq org-publish-project-alist
      (list
       (list "sources"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["sources.org"]
             :publishing-function '(org-html-publish-to-html)
             :publishing-directory "./public")
       (list "files"
             :base-directory "./files"
             :base-extension "org"
             :publishing-directory "./public/files"
             :publishing-function '(org-babel-tangle-publish))
       (list "sources-all"
             :components '("sources" "files"))))

(provide 'publish)
