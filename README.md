# Ph.D. thesis of Marek Felšöci: sources

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/sources/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/sources/-/commits/master)

This repository contains a literate description of the source code involved in
the construction of experimental environment as well as the definition and the
computation of experiments within the
[Ph.D. thesis of Marek Felšöci](https://gitlab.inria.fr/thesis-mfelsoci/thesis).

It is meant to be re-used as a submodule in different experimental studies.

Check the literate description of the source code in this repository online by
following the link below.

<a href="https://thesis-mfelsoci.gitlabpages.inria.fr/sources" target="_blank">
  <strong>https://thesis-mfelsoci.gitlabpages.inria.fr/sources</strong>
</a>
