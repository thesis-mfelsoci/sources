# -*- org-export-babel-evaluate: nil -*-
#+SETUPFILE: ./readtheorginria.setup
#+OPTIONS: timestamp:nil email:t htmlize:t ^:{} H:4
#+STARTUP: showeverything
#+MACRO: ref_gcvb [[#benchmarks]]
#+MACRO: ref_definition_file [[#benchmarks]]
#+MACRO: ref_sbatch_templates [[#benchmarks]]
#+MACRO: qrm ~qr_mumps~
#+MACRO: gcvb ~gcvb~
#+MACRO: slurm /Slurm/
#+TITLE: Ph.D. thesis of Marek Felšöci: Source files
#+AUTHOR: Emmanuel Agullo, Marek Felšöci, Guillaume Sylvand
#+EMAIL: emmanuel.agullo@inria.fr, marek.felsoci@inria.fr, guillaume.sylvand@airbus.com
#+DATE: {{{time(%d/%m/%Y | %H:%M:%S)}}}

Reproducibility of numerical experiments has always been a complex matter.
Building exactly the same software environment on multiple computing platforms
may be long, tedious and sometimes virtually impossible to be done manually.

Within our research work, we put strong emphasis on ensuring the reproducibility
of the experimental environment. On one hand, we make use of the GNU Guix
cite:guix transactional package manager allowing for a complete reproducibility
of software environments across different machines. On the other hand, we rely
on the principles of literate programming cite:Knuth84 in an effort to maintain
an exhaustive, clear and accessible documentation of our experiments and the
associated environment.

This document represents such a documentation for the source files commonly
required for constructing our experimental environments and running the
experiments. It is written using Org mode for Emacs cite:Dominik18,emacs which
defines the Org markup language allowing to combine formatted text, images and
figures with traditional source code. The =.org= source this document is based
on features the source code blocks of multiple scripts and configuration files
that can to be extracted cite:OrgTangle into separate source files [fn:files].

Eventually, the document can be exported to various output formats
cite:OrgExport such as the HTML page you are currently viewing.

[fn:files] The source of this document as well as the source code files
described in the latter are available for download at:
[[https://thesis-mfelsoci.gitlabpages.inria.fr/sources/files.tar.gz]]


#+INCLUDE: "./files/getsrc.org" :minlevel 1

** Defining, scheduling and computing benchmarks
:PROPERTIES:
:CUSTOM_ID: benchmarks
:END:

To automatize the generation and the computation of benchmarks, we use ~gcvb~
cite:gcvb, an open-source tool developed at Airbus. ~gcvb~ allows us to define
benchmarks, generate corresponding shell job scripts for every benchmark or a
selected group of benchmarks, submit these job scripts for execution, then
gather and optionally validate or post-process the results. To generate multiple
variants of the same benchmark, ~gcvb~ supports templates.

The configuration of ~gcvb~, the benchmark definitions as well as scheduling
parameters are specific to a given experimental study. We thus describe these in
the study-specific documentations. Follow
[[https://thesis-mfelsoci.gitlabpages.inria.fr/thesis/][this link]] to see the
list of all of our experimental studies.

#+INCLUDE: "./files/rss.org" :minlevel 1

#+INCLUDE: "./files/es_config.org" :minlevel 1

#+INCLUDE: "./files/parse.org" :minlevel 1

#+INCLUDE: "./files/inject.org" :minlevel 1

#+INCLUDE: "./files/wrapper.org" :minlevel 1

#+INCLUDE: "./files/submit.org" :minlevel 1

#+INCLUDE: "./files/plot.org" :minlevel 1

#+INCLUDE: "./files/starvz.org" :minlevel 1

bibliography:bibliography.bib
bibliographystyle:siam
